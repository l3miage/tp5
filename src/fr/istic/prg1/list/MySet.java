package fr.istic.prg1.list;

import fr.istic.prg1.list_util.Comparison;
import fr.istic.prg1.list_util.List;
import fr.istic.prg1.list_util.SmallSet;
import fr.istic.prg1.list_util.Iterator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

/**
 * @author Mickael Foursov <foursov@univ-rennes1.fr>
 * @version 5.0
 * @since 2018-10-02
 */

public class MySet extends List<SubSet> {

    private static final int MAX_RANG = 128;
    private static final SubSet FLAG_VALUE = new SubSet(MAX_RANG, new SmallSet());
    private static final Scanner standardInput = new Scanner(System.in);

    public MySet() {
        super();
        setFlag(FLAG_VALUE);
    }

    public static void closeAll() {
        standardInput.close();
    }

    private static Comparison compare(int a, int b) {
        if (a < b) {
            return Comparison.INF;
        } else if (a == b) {
            return Comparison.EGAL;
        } else {
            return Comparison.SUP;
        }
    }

    public void print() {
        System.out.println(" [version corrigee de contenu]");
        this.print(System.out);
    }

    // //////////////////////////////////////////////////////////////////////////////
    // //////////// Appartenance, Ajout, Suppression, Cardinal
    // ////////////////////
    // //////////////////////////////////////////////////////////////////////////////

    /**
     * Ajouter a this toutes les valeurs saisies par l'utilisateur et afficher
     * le nouveau contenu (arret par lecture de -1).
     */
    public void add() {
        System.out.println(" valeurs a ajouter (-1 pour finir) : ");
        this.add(System.in);
        System.out.println(" nouveau contenu :");
        this.printNewState();
    }

    /**
     * Ajouter a this toutes les valeurs prises dans is.
     * C'est une fonction auxiliaire pour add() et restore().
     *
     * @param is flux d'entree.
     */
    public void add(InputStream is) {
        Scanner scanner = new Scanner(is);
        int x;
        if (scanner.hasNextInt()) x = scanner.nextInt();
        else x = -1;
        while (x != -1) {
            addNumber(x);
            if (scanner.hasNextInt()) x = scanner.nextInt();
            else x = -1;
        }
        scanner.close();
    }

    /**
     * Ajouter value a this.
     *
     * @param value valuer a ajouter.
     */
    public void addNumber(int value) {
        if (value >= 0 && value <= 32767) {
            Iterator<SubSet> it = this.iterator();
            while (!it.isOnFlag() && it.getValue().rank < value / 256) {
                it.goForward();
            }

            // Si c'est le bon rang alors on ajout l'élément
            if (it.getValue().rank == value / 256) it.getValue().set.add(value % 256);
            else { // Sinon on crée le subset et on l'ajoute sur l'iterator
                SubSet subSet = new SubSet(value / 256, new SmallSet());
                subSet.set.add(value % 256);
                it.addLeft(subSet);
            }
        }
    }

    /**
     * Supprimer de this toutes les valeurs saisies par l'utilisateur et
     * afficher le nouveau contenu (arret par lecture de -1).
     */
    public void remove() {
        System.out.println("  valeurs a supprimer (-1 pour finir) : ");
        this.remove(System.in);
        System.out.println(" nouveau contenu :");
        this.printNewState();
    }

    /**
     * Supprimer de this toutes les valeurs prises dans is.
     *
     * @param is flux d'entree
     */
    public void remove(InputStream is) {
        Scanner scanner = new Scanner(is);
        int x;
        if (scanner.hasNextInt()) x = scanner.nextInt();
        else x = -1;
        while (x != -1) {
            removeNumber(x);
            if (scanner.hasNextInt()) x = scanner.nextInt();
            else x = -1;
        }
        scanner.close();
    }

    /**
     * Supprimer value de this.
     *
     * @param value valeur a supprimer
     */
    public void removeNumber(int value) {
        if (value >= 0 && value <= 32767) {
            Iterator<SubSet> it = this.iterator();
            while (!it.isOnFlag() && it.getValue().rank < value / 256) {
                it.goForward();
            }

            // Si on est sur le bon subset
            if (it.getValue().rank == value / 256) {
                it.getValue().set.remove(value % 256);
                if (it.getValue().set.isEmpty()) it.remove();
            }
        }
    }

    /**
     * @return taille de l'ensemble this
     */
    public int size() {
        int ret = 0;
        Iterator<SubSet> it = this.iterator();
        // Recherche dans tous l'ensemble
        while (!it.isOnFlag()) {
            ret += it.getValue().set.size();
            it.goForward();
        }
        return ret;
    }


    /**
     * @return true si le nombre saisi par l'utilisateur appartient a this,
     * false sinon
     */
    public boolean contains() {
        System.out.println(" valeur cherchee : ");
        return this.containsAux(readValue(standardInput, 0));
    }

    public boolean containsAux(int x) {
        if (x <= 32767 && x >= 0) {
            Iterator<SubSet> it = this.iterator();
            while (!it.isOnFlag()) {
                if (it.getValue().rank == x / 256) {
                    return it.getValue().set.contains(x % 256);
                }
                it.goForward();
            }
        }
        return false;
    }

    // /////////////////////////////////////////////////////////////////////////////
    // /////// Difference, DifferenceSymetrique, Intersection, Union ///////
    // /////////////////////////////////////////////////////////////////////////////

    /**
     * This devient la difference de this et set2.
     *
     * @param set2 deuxieme ensemble
     */
    public void difference(MySet set2) {
        Iterator<SubSet> it1 = this.iterator();
        Iterator<SubSet> it2 = set2.iterator();

        if (this.equals(set2)) {
            while (!it1.isOnFlag()) {
                it1.remove();
            }
        } else {
            while (!it1.isOnFlag()) {
                if (it1.getValue().rank == it2.getValue().rank) { // Si on est sur le meme rank
                    it1.getValue().set.difference(it2.getValue().set);
                    if (it1.getValue().set.isEmpty()) it1.remove();
                    else it1.goForward();
                    it2.goForward();
                } else if (it1.getValue().rank > it2.getValue().rank) it2.goForward(); // Ajustement de l'iterator
                else it1.goForward();
            }
        }
    }

    /**
     * This devient la difference symetrique de this et set2.
     *
     * @param set2 deuxieme ensemble
     */
    public void symmetricDifference(MySet set2) {
        Iterator<SubSet> it1 = this.iterator();
        Iterator<SubSet> it2 = set2.iterator();

        if (this.equals(set2)) {
            while (!it1.isOnFlag()) {
                it1.remove();
            }
        } else {
            while (!it2.isOnFlag()) {
                if (it1.getValue().rank == it2.getValue().rank) {
                    it1.getValue().set.symmetricDifference(it2.getValue().set);
                    if (it1.getValue().set.isEmpty()) it1.remove();
                    else it1.goForward();
                    it2.goForward();
                } else if (it1.getValue().rank > it2.getValue().rank) {
                    it1.addLeft(it2.getValue().clone());
                    it2.goForward();
                } else it1.goForward();
            }
        }
    }

    /**
     * This devient l'intersection de this et set2.
     *
     * @param set2 deuxieme ensemble
     */
    public void intersection(MySet set2) {
        Iterator<SubSet> it1 = this.iterator();
        Iterator<SubSet> it2 = set2.iterator();

        while (!it1.isOnFlag()) {
            if (it1.getValue().rank == it2.getValue().rank) {
                it1.getValue().set.intersection(it2.getValue().set);
                if (it1.getValue().set.isEmpty()) it1.remove();
                else it1.goForward();
                it2.goForward();
            } else if (it1.getValue().rank > it2.getValue().rank) it2.goForward();
            else it1.remove();
        }
    }

    /**
     * This devient l'union de this et set2.
     *
     * @param set2 deuxieme ensemble
     */
    public void union(MySet set2) {
        Iterator<SubSet> it1 = this.iterator();
        Iterator<SubSet> it2 = set2.iterator();

        while (!it2.isOnFlag()) {
            if (it1.getValue().rank == it2.getValue().rank) {
                it1.getValue().set.union(it2.getValue().set);
                it1.goForward();
                it2.goForward();
            } else if (it1.getValue().rank > it2.getValue().rank) {
                it1.addLeft(it2.getValue().clone());
                it2.goForward();
            } else it1.goForward();
        }
    }

    // /////////////////////////////////////////////////////////////////////////////
    // /////////////////// Egalite, Inclusion ////////////////////
    // /////////////////////////////////////////////////////////////////////////////

    /**
     * @param o deuxieme ensemble
     * @return true si les ensembles this et o sont egaux, false sinon
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null) {
            return false;
        } else if (!(o instanceof MySet)) {
            return false;
        } else { // Sinon O est un MySet non nul donc on compare chaque valeur de this et O
            MySet mySet = (MySet) o;
            Iterator<SubSet> it1 = this.iterator();
            Iterator<SubSet> it2 = mySet.iterator();

            while (!it1.isOnFlag() || !it2.isOnFlag()) {
                if (!it1.getValue().equals(it2.getValue())) return false;
                it1.goForward();
                it2.goForward();
            }
        }
        return true;
    }

    /**
     * @param set2 deuxieme ensemble
     * @return true si this est inclus dans set2, false sinon
     */
    public boolean isIncludedIn(MySet set2) {
        Iterator<SubSet> it1 = this.iterator();
        Iterator<SubSet> it2 = set2.iterator();

        while (!it1.isOnFlag() || !it2.isOnFlag()) {
            if (it1.getValue().rank == it2.getValue().rank) {
                if (!it1.getValue().set.isIncludedIn(it2.getValue().set)) return false;
                else {
                    it1.goForward();
                    it2.goForward();
                }
            } else if (it1.getValue().rank > it2.getValue().rank) it2.goForward();
            else return false;
        }
        return true;
    }

    // /////////////////////////////////////////////////////////////////////////////
    // //////// Rangs, Restauration, Sauvegarde, Affichage //////////////
    // /////////////////////////////////////////////////////////////////////////////

    /**
     * Afficher les rangs presents dans this.
     */
    public void printRanks() {
        System.out.println(" [version corrigee de rangs]");
        this.printRanksAux();
    }

    private void printRanksAux() {
        int count = 0;
        System.out.println(" Rangs presents :");
        Iterator<SubSet> it = this.iterator();
        while (!it.isOnFlag()) {
            System.out.print("" + it.getValue().rank + "  ");
            count = count + 1;
            if (count == 10) {
                System.out.println();
                count = 0;
            }
            it.goForward();
        }
        if (count > 0) {
            System.out.println();
        }
    }

    /**
     * Creer this a partir d'un fichier choisi par l'utilisateur contenant une
     * sequence d'entiers positifs terminee par -1 (cf f0.ens, f1.ens, f2.ens,
     * f3.ens et f4.ens).
     */
    public void restore() {
        String fileName = readFileName();
        InputStream inFile;
        try {
            inFile = new FileInputStream(fileName);
            System.out.println(" [version corrigee de restauration]");
            this.clear();
            this.add(inFile);
            inFile.close();
            System.out.println(" nouveau contenu :");
            this.printNewState();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("fichier " + fileName + " inexistant");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("probleme de fermeture du fichier " + fileName);
        }
    }

    /**
     * Sauvegarder this dans un fichier d'entiers positifs termine par -1.
     */
    public void save() {
        System.out.println(" [version corrigee de sauvegarde]");
        OutputStream outFile;
        try {
            outFile = new FileOutputStream(readFileName());
            this.print(outFile);
            outFile.write("-1\n".getBytes());
            outFile.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("pb ouverture fichier lors de la sauvegarde");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("probleme de fermeture du fichier");
        }
    }

    /**
     * @return l'ensemble this sous forme de chaine de caracteres.
     */
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        int count = 0;
        SubSet subSet;
        int startValue;
        Iterator<SubSet> it = this.iterator();
        while (!it.isOnFlag()) {
            subSet = it.getValue();
            startValue = subSet.rank * 256;
            for (int i = 0; i < 256; ++i) {
                if (subSet.set.contains(i)) {
                    String number = String.valueOf(startValue + i);
                    int numberLength = number.length();
                    for (int j = 6; j > numberLength; --j) {
                        number += " ";
                    }
                    result.append(number);
                    ++count;
                    if (count == 10) {
                        result.append("\n");
                        count = 0;
                    }
                }
            }
            it.goForward();
        }
        if (count > 0) {
            result.append("\n");
        }
        return result.toString();
    }

    /**
     * Imprimer this dans outFile.
     *
     * @param outFile flux de sortie
     */
    private void print(OutputStream outFile) {
        try {
            String string = this.toString();
            outFile.write(string.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Afficher l'ensemble avec sa taille et les rangs presents.
     */
    private void printNewState() {
        this.print(System.out);
        System.out.println(" Nombre d'elements : " + this.size());
        this.printRanksAux();
    }

    /**
     * @param scanner
     * @param min     valeur minimale possible
     * @return l'entier lu au clavier (doit être entre min et 32767)
     */
    private static int readValue(Scanner scanner, int min) {
        int value = scanner.nextInt();
        while (value < min || value > 32767) {
            System.out.println("valeur incorrecte");
            value = scanner.nextInt();
        }
        return value;
    }

    /**
     * @return nom de fichier saisi psar l'utilisateur
     */
    private static String readFileName() {
        System.out.print(" nom du fichier : ");
        return standardInput.next();
    }
}