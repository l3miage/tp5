package fr.istic.prg1.list;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class List<T> {

    private final List<T>.ListIterator flag;
    private final Set<IteratorList> set = new HashSet<>();

    public List() {
        this.flag = new ListIterator();
        this.flag.right = this.flag;
        this.flag.left = this.flag;
    }

    public IteratorList iterator() {
        IteratorList it = new IteratorList();
        this.set.add(it);
        return it;
    }

    public boolean isEmpty() {
        return this.flag.left == this.flag;
    }

    public void clear() {
        this.flag.right = this.flag;
        this.flag.left = this.flag;

        for (IteratorList it : this.set) {
            it.value = this.flag;
        }
    }

    public void setFlag(T v) {
        this.flag.element = v;
    }

    public void addHead(T v) {
        IteratorList it = this.iterator();
        it.addLeft(v);
        it.selfDestroy();
    }

    public void addTail(T v) {
        IteratorList it = this.iterator();
        it.goBackward();
        it.addLeft(v);
        it.selfDestroy();
    }

    public List<T> clone() {
        List<T> nouvListe = new List<>();

        try {
            IteratorList p = this.iterator();
            Method method = p.getValue().getClass().getMethod("clone");

            while (!p.isOnFlag()) {
                nouvListe.addTail((T) method.invoke(p.getValue()));
                p.goForward();
            }
        } catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | IllegalAccessException var4) {
            var4.printStackTrace();
        }

        return nouvListe;
    }

    public String toString() {
        String s = "contenu de la liste : \n";
        IteratorList p = this.iterator();

        while (!p.isOnFlag()) {
            s = s + p.getValue().toString() + " ";
            p.goForward();
        }

        return s;
    }

    private class ListIterator {
        public T element = null;
        public ListIterator right = null;
        public ListIterator left = null;

        public ListIterator() {
        }
    }

    public class IteratorList implements Iterator<T> {
        private ListIterator value;

        private IteratorList() {
            this.value = List.this.flag.left;
        }

        public void goForward() {
            try {
                assert this.value.left != null : "\n\n\nimpossible d'avancer : le voisin droit n'existe pas\n\n\n";
            } catch (AssertionError var2) {
                var2.printStackTrace();
                System.exit(0);
            }

            this.value = this.value.left;
        }

        public void goBackward() {
            try {
                assert this.value.right != null : "\n\n\nimpossible de reculer : le voisin gauche n'existe pas\n\n\n";
            } catch (AssertionError var2) {
                var2.printStackTrace();
                System.exit(0);
            }

            this.value = this.value.right;
        }

        public void restart() {
            this.value = List.this.flag.left;
        }

        public boolean isOnFlag() {
            return this.value == List.this.flag;
        }

        @Override
        public boolean hasNext() {
            return false;
        }

        @Override
        public T next() {
            return null;
        }

        public void remove() {
            try {
                assert this.value != List.this.flag : "\n\n\nimpossible de retirer le drapeau\n\n\n";
            } catch (AssertionError var4) {
                var4.printStackTrace();
                System.exit(0);
            }

            ListIterator temp = this.value;
            ListIterator leftNeighbor = this.value.right;
            ListIterator rightNeighbor = this.value.left;
            leftNeighbor.left = rightNeighbor;
            rightNeighbor.right = leftNeighbor;
            this.value = this.value.left;
            temp.right = null;
            temp.left = null;
        }

        public T getValue() {
            try {
                assert this.value.element != null : "\n\n\nl'element courant n'a pas de valeur\n\n\n";
            } catch (AssertionError var2) {
                var2.printStackTrace();
                System.exit(0);
            }

            return this.value.element;
        }

        public T nextValue() {
            try {
                assert this.value.left != null : "\n\n\nimpossible d'avancer : le voisin droit n'existe pas\n\n\n";
            } catch (AssertionError var2) {
                var2.printStackTrace();
                System.exit(0);
            }

            this.goForward();
            return this.getValue();
        }

        public void addLeft(T v) {
            ListIterator leftNeighbor = this.value.right;
            ListIterator rightNeighbor = this.value;
            this.value = new ListIterator();
            this.value.element = v;
            this.value.right = leftNeighbor;
            this.value.left = rightNeighbor;
            leftNeighbor.left = this.value;
            rightNeighbor.right = this.value;
        }

        public void addRight(T v) {
            this.goForward();
            this.addLeft(v);
        }

        public void setValue(T v) {
            this.value.element = v;
        }

        public String toString() {
            return "parcours de liste : pas d'affichage possible \n";
        }

        public void selfDestroy() {
            List.this.set.remove(this);
            this.value = null;
        }
    }

}